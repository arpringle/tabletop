# Tabletop

*Tabletop* is a character manager for tabletop role-playing games. It helps with keeping track of your player and non-player characters, managing their inventories, statistics, and more for you.

It has the potential to work with many different role-playing game systems, and is extensible with new content modules using XML definitions. Information on the XML spec is in the /spec folder.

Tabletop is made available under the GNU General Public License, Version 3 or later, to secure your freedom to use the software.