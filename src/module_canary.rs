/* module_canary.rs
 *
 * Copyright 2024 Austin Pringle
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use gtk::prelude::*;
use adw::subclass::prelude::*;
use gtk::{gio, glib};

use libxml;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct TabletopModuleCanary {
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TabletopModuleCanary {
        const NAME: &'static str = "TabletopModuleCanary";
        type Type = super::TabletopModuleCanary;
        type ParentType = glib::Object;
    }

    impl TabletopModuleCanary {
        pub fn dispatch_validator(&self, system_identifier:u32){
            return;
        }
    }

    impl ObjectImpl for TabletopModuleCanary {}

}

glib::wrapper! {
    pub struct TabletopModuleCanary(ObjectSubclass<imp::TabletopModuleCanary>);
}

impl TabletopModuleCanary {
    pub fn new() -> Self {
        glib::Object::builder()
            .build()
    }
}


