/* application.rs
 *
 * Copyright 2024 Austin Pringle
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use gtk::prelude::*;
use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::{gio, glib};

use crate::config::VERSION;
use crate::TabletopWindow;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct TabletopApplication {}

    #[glib::object_subclass]
    impl ObjectSubclass for TabletopApplication {
        const NAME: &'static str = "TabletopApplication";
        type Type = super::TabletopApplication;
        type ParentType = adw::Application;
    }

    impl ObjectImpl for TabletopApplication {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();
            obj.setup_gactions();
            obj.set_accels_for_action("app.quit", &["<primary>q"]);
        }
    }

    impl ApplicationImpl for TabletopApplication {
        // We connect to the activate callback to create a window when the application
        // has been launched. Additionally, this callback notifies us when the user
        // tries to launch a "second instance" of the application. When they try
        // to do that, we'll just present any existing window.
        fn activate(&self) {

            let application = self.obj();
            // Get the current window or create one if necessary
            let window = if let Some(window) = application.active_window() {
                window
            } else {
                let window = TabletopWindow::new(&*application);
                window.upcast()
            };

            // Ask the window manager/compositor to present the window
            window.present();
        }
    }

    impl GtkApplicationImpl for TabletopApplication {}
    impl AdwApplicationImpl for TabletopApplication {}
}

glib::wrapper! {
    pub struct TabletopApplication(ObjectSubclass<imp::TabletopApplication>)
        @extends gio::Application, gtk::Application, adw::Application,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl TabletopApplication {
    pub fn new(application_id: &str, flags: &gio::ApplicationFlags) -> Self {
        glib::Object::builder()
            .property("application-id", application_id)
            .property("flags", flags)
            .build()
    }

    fn setup_gactions(&self) {
        let quit_action = gio::ActionEntry::builder("quit")
            .activate(move |app: &Self, _, _| app.quit())
            .build();
        let about_action = gio::ActionEntry::builder("about")
            .activate(move |app: &Self, _, _| app.show_about())
            .build();
        let new_campaign_action = gio::ActionEntry::builder("new_campaign")
            .activate(move |app: &Self, _, _| app.create_new_campaign())
            .build();
        self.add_action_entries([quit_action, about_action, new_campaign_action]);
    }

    fn show_about(&self) {
        let window = self.active_window().unwrap();
        let about = adw::AboutDialog::builder()
            .application_name("Tabletop")
            .application_icon("org.adventurerstoolbox.Tabletop")
            .developer_name("Austin Pringle")
            .license_type(gtk::License::Gpl30)
            .version(VERSION)
            .developers(vec!["Austin Pringle"])
            .copyright("© 2024 Austin Pringle")
            .build();
        about.present(&window);
    }

    fn create_new_campaign(&self) {
        let window = self.active_window().unwrap();
        let about = adw::AboutWindow::builder()
            .transient_for(&window)
            .application_name("Tabletop")
            .application_icon("org.adventurerstoolbox.Tabletop")
            .developer_name("Austin Pringle")
            .version(VERSION)
            .developers(vec!["Austin Pringle", "Joe Mamma"])
            .copyright("© 2024 Austin Pringle")
            .build();

        about.present();
    }
}
