/* window.rs
 *
 * Copyright 2024 Austin Pringle
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use gtk::prelude::*;
use adw::subclass::prelude::*;
use gtk::{gio, glib};

use crate::TabletopCampaignCreationPage;

mod imp {
    use super::*;

    #[derive(Debug, Default, gtk::CompositeTemplate)]
    #[template(resource = "/org/adventurerstoolbox/Tabletop/window.ui")]
    pub struct TabletopWindow {
        #[template_child]
        pub navigation_view: TemplateChild<adw::NavigationView>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TabletopWindow {
        const NAME: &'static str = "TabletopWindow";
        type Type = super::TabletopWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[gtk::template_callbacks]
    impl TabletopWindow {
        #[template_callback]
        fn create_new_campaign_button_clicked(&self){
            let campaign_creation_page = TabletopCampaignCreationPage::new();
            self.navigation_view.push(&campaign_creation_page);
        }
    }

    impl ObjectImpl for TabletopWindow {}
    impl WidgetImpl for TabletopWindow {}
    impl WindowImpl for TabletopWindow {}
    impl ApplicationWindowImpl for TabletopWindow {}
    impl AdwApplicationWindowImpl for TabletopWindow {}
}

glib::wrapper! {
    pub struct TabletopWindow(ObjectSubclass<imp::TabletopWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow, @implements gio::ActionGroup, gio::ActionMap;
}

impl TabletopWindow {
    pub fn new<P: IsA<gtk::Application>>(application: &P) -> Self {
        glib::Object::builder()
            .property("application", application)
            .build()
    }
}


