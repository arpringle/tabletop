/* content_check_row.rs
 *
 * Copyright 2024 Austin Pringle
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use gtk::prelude::*;
use adw::subclass::prelude::*;
use gtk::{gio, glib};

mod imp {
    use super::*;

    #[derive(Debug, Default, gtk::CompositeTemplate)]
    #[template(resource = "/org/adventurerstoolbox/Tabletop/content_check_row.ui")]
    pub struct TabletopContentCheckRow {
        #[template_child]
        pub check_button: TemplateChild<gtk::CheckButton>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TabletopContentCheckRow {
        const NAME: &'static str = "TabletopContentCheckRow";
        type Type = super::TabletopContentCheckRow;
        type ParentType = adw::ActionRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[gtk::template_callbacks]
    impl TabletopContentCheckRow {
        #[template_callback]
        fn content_check_row_check_button_checked(button: gtk::Button){
            glib::g_print!("test");
        }
    }

    impl ObjectImpl for TabletopContentCheckRow {}
    impl WidgetImpl for TabletopContentCheckRow {}
    impl ListBoxRowImpl for TabletopContentCheckRow {}
    impl PreferencesRowImpl for TabletopContentCheckRow {}
    impl ActionRowImpl for TabletopContentCheckRow {}

}

glib::wrapper! {
    pub struct TabletopContentCheckRow(ObjectSubclass<imp::TabletopContentCheckRow>)
        @extends gtk::Widget, gtk::ListBoxRow, adw::PreferencesRow, adw::ActionRow, @implements gtk::Actionable;
}

impl TabletopContentCheckRow {
    pub fn new() -> Self {
        glib::Object::builder()
            .build()
    }
}
