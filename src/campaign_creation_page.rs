/* campaign_creation_page.rs
 *
 * Copyright 2024 Austin Pringle
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use gtk::prelude::*;
use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::{gio, glib};

use crate::TabletopModuleCanary;

mod imp {
    use super::*;

    #[derive(Debug, Default, gtk::CompositeTemplate)]
    #[template(resource = "/org/adventurerstoolbox/Tabletop/campaign_creation_page.ui")]
    pub struct TabletopCampaignCreationPage {
        #[template_child]
        pub content_settings_group: TemplateChild<adw::PreferencesGroup>,
        #[template_child]
        pub system_combo_row: TemplateChild<adw::ComboRow>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TabletopCampaignCreationPage {
        const NAME: &'static str = "TabletopCampaignCreationPage";
        type Type = super::TabletopCampaignCreationPage;
        type ParentType = adw::NavigationPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[gtk::template_callbacks]
    impl TabletopCampaignCreationPage {

        pub fn fetch_modules(&self){
            let canary = TabletopModuleCanary::new();
            let selection = self.system_combo_row.selected();
            canary.imp().dispatch_validator(selection);
        }

        #[template_callback]
        fn system_combo_row_changed(&self){
            self.fetch_modules();
        }
    }

    impl ObjectImpl for TabletopCampaignCreationPage {}
    impl WidgetImpl for TabletopCampaignCreationPage {}
    impl NavigationPageImpl for TabletopCampaignCreationPage {}
}

glib::wrapper! {
    pub struct TabletopCampaignCreationPage(ObjectSubclass<imp::TabletopCampaignCreationPage>)
        @extends gtk::Widget, adw::NavigationPage;
}

impl TabletopCampaignCreationPage {
    pub fn new() -> Self {
        let campaign_creation_page: TabletopCampaignCreationPage = glib::Object::builder().build();
        campaign_creation_page.imp().fetch_modules();
        return campaign_creation_page;
    }
}
