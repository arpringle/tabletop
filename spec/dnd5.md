# `.ttmod` File Specification - Dungeons & Dragons, Fifth Edition Usage
<br>

> NOTICE - This work includes material taken from the System Reference Document 5.1 (“SRD 5.1”) by Wizards of the Coast LLC and available at <https://dnd.wizards.com/resources/systems-reference-document>. The SRD 5.1 is licensed under the Creative Commons Attribution 4.0 International License available at <https://creativecommons.org/licenses/by/4.0/legalcode>.

## Intro

This file contains the specification for writing `.ttmod` files to add new content designed for Dungeons & Dragons, Fifth Edition into the _Tabletop_ software. The general aspects of the specification can be found elsewhere.

To declare a new D&D module, you must use the standard XML preamble, declare the `<module>` tag, give the module a name via the `name` attribute, and set the `system` attribute of the `<module>` tag to be "`dnd5`":

```xml
<?xml version="1.0" encoding="UTF-8"?>
<module name="My Amazing Module" system="dnd5">

<!--Module contents go here-->

</module>
```

Inside of the module tag, you will place the content for your module. In order to do this, you must declare the various categories of content that the module adds:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<module name="My Amazing Module" system="dnd5">
	<items>
		<!--Items go here-->
	</items>
	<races>
		<!--Races go here-->
	</races>
</module>
```

Next, you would put specific pieces of content inside of the content category tags. The tags for the specific content are the singular form of the tags for the category. (So if the category tag is `<races>`, the individual races would each be tagged `<race>`.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<module name="My Amazing Module" system="dnd5">
	<items>
		<item name="My Potion" type="aid">
			<!--Details about "my potion" go here-->
		</item>
		<item name="My Sword" type="weapon">
			<!--Details about "my sword" go here-->
		</item>
	</items>
	<races>
		<race name="Super Mutant">
			<!--Details about "super mutant" go here-->
		</race>
	</races>
</module>
```

A listing of all of the content categories supported for D&D is as follows:

- `<backgrounds>` for character backgrounds
- `<classes>` for character classes
- `<creatures>` for creatures/enemies/monsters
- `<feats>` for character feats
- `<items>` for items
- `<races>` for character races
- `<spells>` for spells

## Backgrounds

Declare that you are going to list player backgrounds by using the `<backgrounds>` tag inside of your `<module>` tag:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<module name="My Backgrounds Module" system="dnd5">
   <backgrounds>
		<!--Backgrounds go here-->
	</backgrounds>
</module>
```

Each `<background>` tag child of the `<backgrounds>` tag must include the `name` attribute.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<module name="My Backgrounds Module" system="dnd5">
   <backgrounds>
		<background name="Acolyte">
			<!--Acolyte details go here-->
		</background>
	</backgrounds>
</module>
```

The tags that can be children of the `<background>` tag are as follows:

- `<description>` for the background description.
- `<equipment>` to list what items the background affords.
- `<features>` to list the names and descriptions of the background's features.
- `<item-proficiencies>` to list what tool/weapon proficiencies the background affords.
- `<skill-proficiencies>` to list what skill proficiencies are given by the background.
- `<traits>` for non-mechanical aspects of the background.

For quick reference, here is what the D&D SRD's "Acolyte" background would look like as a `.ttmod` file (it's a big one). The tags are explained in detail afterwards:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<module name="My Backgrounds Module" system="dnd5">
   <backgrounds>
		<background name="Acolyte">
			<description>
You have spent your life in the service of a temple
to a specific god or pantheon of gods. You act as an
intermediary between the realm of the holy and the
mortal world, performing sacred rites and offering
sacrifices in order to conduct worshipers into the
presence of the divine. You are not necessarily a
cleric—performing sacred rites is not the same thing
as channeling divine power.
[br]
Choose a god, a pantheon of gods, or some other
quasi—divine being from among those listed in
"Fantasy—Historical Pantheons" or those specified by
your GM, and work with your GM to detail the nature
of your religious service. Were you a lesser
functionary in a temple, raised from childhood to
assist the priests in the sacred rites? Or were you a
high priest who suddenly experienced a call to serve
your god in a different way? Perhaps you were the
leader of a small cult outside of any established
temple structure, or even an occult group that
served a fiendish master that you now deny.
			</description>
			<equipment>
				<gear>Holy Symbol</gear>
				<pick>
			   		<gear>Prayer Book</gear>
					<gear>Prayer Wheel</gear>
				</pick>
				<gear quantity="5">Stick of Incense</gear> 
				<gear>Vestments</gear>
				<gear>Common Clothes</gear>
				<money>
					<gold>15</gold>
				</money>
			</equipment>
			<features>
				<feature name="Shelter of the Faithful">
As an acolyte, you command the respect of those who
share your faith, and you can perform the religious
ceremonies of your deity. You and your
adventuring companions can expect to receive free
healing and care at a temple, shrine, or other
established presence of your faith, though you must
provide any material components needed for spells.
Those who share your religion will support you (but
only you) at a modest lifestyle.
<br/>
You might also have ties to a specific temple
dedicated to your chosen deity or pantheon, and you
have a residence there. This could be the temple
where you used to serve, if you remain on good
terms with it, or a temple where you have found a
new home. While near your temple, you can call
upon the priests for assistance, provided the
assistance you ask for is not hazardous and you
remain in good standing with your temple.
				</feature>
			</features>
			<skill-proficiencies>
				<skill-proficiency>Insight</skill-proficiency>
				<skill-proficiency>Religion</skill-proficiency>
			</skill-proficiencies>
			<traits>
				<trait name="Personality Trait">
					<optional-roll dice="1d8">
						<one>
I idolize a particular hero of my faith, and
constantly refer to that person’s deeds and example.
						</one>
						<two>
I can find common ground between the fiercest
enemies, empathizing with them and always
working toward peace.
						</two>
						<three>
I see omens in every event and action. The gods try
to speak to us, we just need to listen.
						</three>
						<four>
Nothing can shake my optimistic attitude.
						</four>
						<five>
I quote (or misquote) sacred texts and proverbs in
almost every situation.			
						</five>
						<six>
I am tolerant (or intolerant) of other faiths and
respect (or condemn) the worship of other gods.
						</six>
						<seven>
I’ve enjoyed fine food, drink, and high society
among my temple’s elite. Rough living grates on me.
						</seven>
						<eight>
I’ve spent so long in the temple that I have little
practical experience dealing with people in the
outside world.
						</eight>
					</optional-roll>
				</trait>
				<trait name="Ideal">
					<optional-roll dice="1d6">
						<one>
Tradition. The ancient traditions of worship and
sacrifice must be preserved and upheld. (Lawful)
						</one>
						<two>
Charity. I always try to help those in need, no matter
what the personal cost. (Good)
						</two>
						<three>
Change. We must help bring about the changes the
gods are constantly working in the world. (Chaotic)
						</three>
						<four>
Power. I hope to one day rise to the top of my faith’s
religious hierarchy. (Lawful)
						</four>
						<five>
Faith. I trust that my deity will guide my actions. I
have faith that if I work hard, things will go well.
(Lawful)
						</five>
						<six>
Aspiration. I seek to prove myself worthy of my
god’s favor by matching my actions against his or
her teachings. (Any)
						</six>
					</optional-roll>
				</trait>
				<trait name="Bond">
					<optional-roll dice="1d6">
						<one>
I would die to recover an ancient relic of my faith
that was lost long ago.
						</one>
						<two>
I will someday get revenge on the corrupt temple
hierarchy who branded me a heretic.
						</two>
						<three>
I owe my life to the priest who took me in when my
parents died.
						</three>
						<four>
Everything I do is for the common people.
						</four>
						<five>
I will do anything to protect the temple where I
served.
						</five>
						<six>
I seek to preserve a sacred text that my enemies
consider heretical and seek to destroy.
						</six>
					</optional-roll>
				</trait>
				<trait name="Flaw">
					<optional-roll dice="1d6">
						<one>
I judge others harshly, and myself even more
severely.
						</one>
						<two>
I put too much trust in those who wield power
within my temple’s hierarchy.
						</two>
						<three>
My piety sometimes leads me to blindly trust those
that profess faith in my god.
						</three>
						<four>
I am inflexible in my thinking.
						</four>
						<five>
I am suspicious of strangers and expect the worst of
them.
						</five>
						<six>
Once I pick a goal, I become obsessed with it to the
detriment of everything else in my life.
						</six>
					</optional-roll>
				</trait>
			</traits>
		</background>
	</backgrounds>
</module>
```
<br>

### Valid Child Tags for the `<background>` Tag:
#### `<description>`
The `<description>` tag comprises the description of the background as it will appear in _Tapletop_. If you are making a module for an existing sourcebook, it is usually just a matter of copying and pasting the description in between the `<description>` tags.

The `<description>` tag has no valid child tags. It only takes a text description. Newlines (pressing enter) are ignored, and paragraph breaks can be represented with `[br]`.

Any variant background options should also be part of the description.

#### `<equipment>`

The `<equipment>` tag is used to indicate what starting equipment is afforded to the character based off of their background.

It takes the `<gear>`, `<money>`, and `<pick>` tags as valid child tags:

- The `<gear>` tag represents an item that a character has. It is not to be confused with the `<item>` tag, which adds new items to a module. In the context of the `<equipment>` tag, `<gear>` represents a piece of starting equipment. `<gear>` takes an optional `quantity` attribute if the character has more than one of the item. The amount of `<gear>` tags as child tags of `<equipment>` is unlimited.

- The `<money>` tag represents an amount of money. Its valid child tags are the types of currency in D&D: `<copper>`, `<silver>`, `<electrum>`, `<gold>`, and `<platinum>`. The content between the opening and closing currency tags must be a number of that type of currency. It can only have of one of each of its child tags. In the context of the `<equipment>` tag, the `<money>` tag represents how much currency is given to a player by their background. There can only be one `<money>` tag as a child tag of `<equipment>`

- The `<pick>` tag allows the player to pick between multiple options. The options are represented by the child tags of the `<pick>` tag. It takes an optional attribute called `amount` which tells how many of the options from the `<pick>` statement the player can pick in total (default is 1). If the `<pick>` tag has its `amount` attribute set to a number that is higher than the number of children, importing your `.ttmod` file to _Tabletop_ will fail. In the context of the `<equipment>` tag, the `<pick>` tag must, in turn, only have `<gear>` tags as children. (The amount of `<pick>` tags as child tags of `<equipment>` is unlimited, and, the amount of `<gear>` tags as child tags of `<pick>` is also unlimited.

#### `<features>`

The `<features>` tag represents gameplay features that aren't mechanical in nature.

The `<features>` tag takes `<feature>` tags as child tags, which, in turn, have text descriptions of the features.

#### `<skill-proficiencies>`

The `<skill-proficiencies>` tag represents which of the standard skills that the background grants you proficiency in.

It takes `<skill-proficiency>` and `<pick>` tags as it's child tags:

- The `<skill-proficiency>` tag represents indivifual skill proficiencies that the character is afforded by the  in turn, have the name of a skill as the content between the opening and closing tag.

- The `<pick>` tag allows the player to pick between multiple options. The options are represented by the child tags of the `<pick>` tag. It takes an optional attribute called `amount` which tells how many of the options from the `<pick>` statement the player can pick in total (default is 1). If the `<pick>` tag has its `amount` attribute set to a number that is higher than the number of children, importing your `.ttmod` file to _Tabletop_ will fail. In the context of the `<skill-proficiencies>` tag, the `<pick>` tag must, in turn, only have `<skill-proficiency>` tags as children. The amount of `<pick>` tags as child tags of `<skill-proficiencies>` is unlimited, and, the amount of `<skill-proficiency>` tags as child tags of `<pick>` is also unlimited.

#### `<item-proficiencies>`
The `<item-proficiencies>` tag represents which items (or categories of items) that the background gives you proficiency in.

It takes `<item-proficiency>` and `<pick>` tags as it's child tags:

- The `<item-proficiency>` tag represents individual tool proficiencies that the character is afforded by the background. The `<item-proficiency>` tags, in turn, have the name of an item as the content between the opening and closing tag.

- The `<pick>` tag allows the player to pick between multiple options. The options are represented by the child tags of the `<pick>` tag. It takes an optional attribute called `amount` which tells how many of the options from the `<pick>` statement the player can pick in total (default is 1). If the `<pick>` tag has its `amount` attribute set to a number that is higher than the number of children, importing your `.ttmod` file to _Tabletop_ will fail. In the context of the `<item-proficiencies>` tag, the `<pick>` tag must, in turn, only have `<item-proficiency>` tags as children. The amount of `<pick>` tags as child tags of `<item-proficiencies>` is unlimited, and, the amount of `<item-proficiency>` tags as child tags of
`<pick>` is also unlimited.

#### `<traits>`

The `<traits>` tag represents non-gameplay, non-mechanical personality traits about your character. It only takes `<trait>` tags as child tags.  `<trait>` tags have a required `name` attribute, describing the name of the trait. Those `<trait>` tags, in turn, only accept a singular `<roll>` or `<optional-roll>` tag as a child:

- The `<roll>` tag represents a dice roll. It has a required attribute called `dice`, which takes a type and number of dice in the formula "{number}d{type}", for example, `<roll dice="1d20">`. It can have two types of children, but either of those children each must have a text description of the trait as it's child. The two acceptable child types for `<roll>` are as follows:
	- It takes phonetically spelled numbers 1-100 as its child tags, for example, `<twenty>`, `<twelve>`, or `<ninetynine>`, and these represent the various outcomes of the dice roll. 
	- Or, it can take child tags written as `<range-result>` with a `range` attribute to represent a range of values that result in a outcome. For example, `<range-result range="1-2">` means, "use the outcome inside these tags if the roll is one or two".
- The `<optional-roll>` tag is the same as the `<roll>` tag, but allows the player to opt for selecting from the potential outcomes instead of forcing them to roll for it.

## Classes

Declare that you are going to list player classes by using the `<classes>` tag inside of your `<module>` tag:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<module name="My Classes Module" system="dnd5">
   <classes>
		<!--Classes go here-->
	</classes>
</module>
```

Each `<class>` tag child of the `<classes>` tag must include the `name` attribute.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<module name="My Classes Module" system="dnd5">
   <classes>
		<class name="Cleric">
			<!--Cleric details go here-->
		</class>
	</classes>
</module>
```

The tags that can be children of the `<class>` tag are as follows:

- `<description>` for the class description.
- `<equipment>` to list the starting items the class affords.
- `<hit-die>` to specify the type of hit die associated with the class.
- `<item-proficiencies>` for defining the specific items or categories of items that the class affords the character proficiency with.
- `<levels>` for representing the progression of a character's abilities and features as they advance in level. It has a complicated syntax in order to detail the features gained at each level of a character class.
- `<pool>` for adding pools of points the character can draw from, or limited-use abilities, ie ki, sorcery points, or "Channel Divinity" Uses.
- `<skill-proficiencies>` for specifying which skills the class affords a character proficiency in.
- `<spellcasting>` for the spellcasting ability of the character class, as well as the.
- `<subclasses>` for the subclasses. The child `<subclass>` tags' content mirrors the structure of the `<class>` tag, but for subclass-exclusive features. 

For quick reference, here is what the D&D SRD's "Cleric" class would look like as a `.ttmod` file (it's a big one). The tags are explained in detail afterwards:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<module name="My Classes Module" system="dnd5">
   <classes>
		<class name="Cleric">
			<description>
As a cleric, you gain the following class features.
			</description>
			<equipment>
				<pick>
					<gear>Mace</gear>
					<gear>Warhammer</gear>
				</pick>
				<pick>
					<gear>Scale Mail</gear>
					<gear>Leather Armor</gear>
					<gear>Chain Mail</gear>
				</pick>
				<pick>
					<gear>Light Crossbow Starter</gear>
					<gear>Simple Weapon</gear>
				</pick>
				<pick>
					<gear>Priest's Pack</gear>
					<gear>Explorer's Pack</gear>
				</pick>
				<gear>Shield</gear>
				<gear>Holy Symbol</gear>
			</equipment>
			<hit-die die="d8"/>
			<item-proficiencies>
				<item-proficiency>Light Armor</item-proficiency>
				<item-proficiency>Medium Armor</item-proficiency>
				<item-proficiency>Shields</item-proficiency>
				<item-proficiency>Simple Weapons</item-proficiency>
			</item-proficiencies>
			<levels>
				<level level="1">
					<unlock-subclass/>
					<cantrips count="3"/>
					<spell-slots level="1" count="2"/>
				</level>
				<level level="2">
					<spell-slots level="1" count="1">
					<pool-ability name="Turn Undead" pool="Channel Divinity Uses">
						<action>
							<target 
							<spell-save ability="WIS">
							<turn-foe type="Undead" hear="true" range="30" see="true"/>
							<>
							</turn-foe>
						</action>
					</pool-ability>
					<pool-increase pool="Channel Divinity Uses" count="1"/>
				</level>
				<level level="3">
					<spell-slots level="1" count="1">
					<spell-slots level="2" count="2">
				</level>
				<level level="4">
					<asi/>
					<cantrips count="1"/>
					<spell-slots level="1" count="1">
					<spell-slots level="2" count="1">
				</level>
				<level level=5>
					<modify
					<>
				</level>
			</levels>
			<pool name="Chanel Divinity Uses" quantity="0" recharge="lr"/>
			<subclasses name="Divine Domain">
				<subclass name="Life Domain">
					<description>
The Life domain focuses on the vibrant positive
energy—one of the fundamental forces of the
universe—that sustains all life. The gods of life
promote vitality and health through healing the sick
and wounded, caring for those in need, and driving
away the forces of death and undeath. Almost any
non-evil deity can claim influence over this domain,
particularly agricultural deities (such as Chauntea,
Arawai, and Demeter), sun gods (such as Lathander,
Pelor, and Re-Horakhty), gods of healing or
endurance (such as Ilmater, Mishakal, Apollo, and
Diancecht), and gods of home and community (such
as Hestia, Hathor, and Boldrei).
					</description>
					<levels>
						<level level="1">
							<spells-learned>
								<spell-learned>Bless</spell-learned>
								<spell-learned>Cure Wounds</spell-learned>
							</spells-learned>						
						</level>
						<level level="2">
							<pool-ability name="Preserve Life" pool="Channel Divinity Uses">
								<action>	
									<heal targets="inf" range="30">!CALC([LVL]*5/[TARGETS])</heal>
								</action>
								<description>
Starting at 2nd level, you can use your Channel
Divinity to heal the badly injured.
[br]
As an action, you present your holy symbol and
evoke healing energy that can restore a number of
hit points equal to five times your cleric level.
Choose any creatures within 30 feet of you, and
divide those hit points among them. This feature can
restore a creature to no more than half of its hit
point maximum. You can’t use this feature on an
undead or a construct.
								</description>
							</pool-ability>						
						</level>
					</levels>
				</subclass>
			</subclasses>
		</class>
	</classes>
</module>
```
