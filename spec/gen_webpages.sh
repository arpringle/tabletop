#!/bin/bash

#I like the rst2html5 generator more than the pandoc or default markdown ones.
#However, I much prefer writing in markdown over rst.
#Therefore, this script generates rst from the markdown using pandoc;
#Then we convert the rst to html using rst2html5.

# Check if there are valid .md files in the current directory
#Specifically this "if" statement lists all files with a .md extension,
#doing so on separate lines, and suppressing errors to /dev/null.
#if there are more than 0 lines, then we know there are .md files.

if [ "$(ls -1 *.md 2>/dev/null | wc -l)" -gt 0 ]; then
    # If valid markdownn files exist, then
    #Create "www" subdirectory if it doesn't already exist
    mkdir -p www

    # Loop through all .md files in the current directory
    for file in *.md; do
        # Check if the file exists and is not empty
        if [ -f "$file" ]; then
            # Remove .md extension from the filename
            filename="${file%.md}"

            # Check if the .html file already exists in the "www" directory
            if [ -f "www/$filename.html" ]; then
            	#If so, delete it.
                rm -f www/$filename.html
            fi

            # Convert Markdown to reStructuredText
            pandoc "$file" -o "www/$filename.rst"

            # Create empty .html file in the "www" directory
            touch "www/$filename.html"

            # Convert reStructuredText to HTML
            rst2html5 "www/$filename.rst" >> "www/$filename.html"

            # Remove the reStructuredText file
            rm -f "www/$filename.rst"
        else
            echo "No md files in directory"
        fi
    done
else
    echo "No valid .md files in the directory"
fi
