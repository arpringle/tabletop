# `.ttmod` File Specification - General Usage
<br>

## Intro

This file contains the general specification for writing `.ttmod` files.

`.ttmod` files represent modules which extend what game content can be accessed from the _Tabletop_ program. This specific file explains the aspects of the specification that apply to _all_ of the supported TTRPG systems. The elements of the specification that are specific to certain TTRPGs are detailed in their own files.

## General File Type Information & A Brief XML Tutorial
`.ttmod` files are __plain text__ files. This means that you can make one in a simple text editor, instead of needing a special editor software. However, it is still possible to make a user-friendly software for generating these files.

Further, `.ttmod` files are written in an __XML__ format. If you already know XML, skip ahead to the next section.

XML stands for __"eXtensible Markup Language"__. XML is usually used for describing text formatting, or the interface of a program, but it is equally useful for describing hierarchical relationships like the ones in these files. It consists of opening and closing "tags", like these:

```xml
<!--Note: these things with the arrows are "comments"; they don't do anything-->

<tag>	<!--This is an opening tag-->
</tag>	<!--This is a closing tag. Note the slash.-->
```

The tags represent an element of the module. Everything in between the tags is the "content" of that element. For another example:

```xml
<tag>THIS IS THE TAG CONTENT</tag>
```

Opening and closing tags can be on the same line, or different lines.

Tags frequently go inside of other tags:

```xml
<!--
In this example, the only tag pair with "text content" is the innermost tag, "yet-another-tag".
The rest of the tag pairs only have tags as their content.
-->

<tag>
	<another-tag>
		<yet-another-tag>The content of the innermost tag</yet-another-tag>
	</another-tag>
</tag>
```

Finally, sometimes, additional info about an element is written in that element's opening tag, like this:
```xml
<tag some-attribute="true">THIS IS THE TAG CONTENT</tag>
```
Those are known as __XML attributes__.

A pro tip: sometimes, tags have no content. In this case, you can save some time by not using a closing tag, and instead closing from the opening tag, like this:
```xml
<!--If you have the following tags, with no content:-->
<tag some-attribute="true"></tag>

<!--It can be abbreviated to this:-->
<tag some-attribute="true"/>
```

## Required Elements
Every `.ttmod` file starts with the standard XML _preamble:_

```xml
<?xml version="1.0" encoding="UTF-8"?>
```

This line stands alone, no opening/closing, etc. It states the XML version (there's only one) and the "text encoding" which is nearly always UTF-8. Unless you've done something super weird to your computer, just copy and paste this as line one of your file.

Now onto the actual XML tags. The "root" of any module starts with the `<module>` tag. There are two attributes that the module tag must have:

1) The `name	` attribute
2) The `system` attribute

The `name` attribute represents what the name of the module will show up as in _Tabletop_. For example, if you were writing a Tabletop module for an existing TTRPG sourcebook, you would probably name the module after that book.

 The `system` attribute represents which of the suported TTRPG systems that the module adds content for. Valid values for the `system` attribute are listed below:

| TTRPG System                     | Value     |
|----------------------------------|-----------|
| Adventurer's Toolbox, 1st Edition|    at     |
| Dungeons & Dragons, 5th Edition  |    dnd5   |
| Pathfinder, 1st Edition          |    pf     |
| Pathfinder, 2nd Edition          |    pf2    |

As an example, the beginnings of a module should look like this:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<module name="My Book of Cool Weapons" system="at">

<!--Module contents go here-->

</module>
```

This is the part common to all modules, however, importing this into _Tabletop_ will fail due to there being no content.

## Adding Content

Content can be added into your module with tags that tell what type of content it is. You use one set of tags to declare a content category, and then you use tag pairs within those tags to declare specific content. Every piece of specific content must have the `name` attribute. 

In the following example, we use the `<items>` tag to let _Tabletop_ know that we are about to list some new game items, but each individual item is enclosed by it's own `<item>` tags:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<module name="My Book of Cool Weapons" system="at">
	<items>
		<item name="The Cool Sword">
		<!--System-dependent details of the item go here-->		
		</item>
		<item name="The Even Cooler Sword">
		<!--System-dependent details of the item go here-->
		</item>
	</items>
</module>
```

The details of the content categories and how they work for the various systems can be found in the spec folder of the Git repository. In the /spec folder, there are files with the specific specifications for each compatible TTRPG system.